from django.contrib.auth.models import User
from django import forms

"""
List of forms 
"""

"""
Form for registration
"""
class UserRegistrationForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('email', 'password')
        labels  = {
            'email': 'E-mail',
            'password': 'Password',
        }

"""
Form for login on site
"""
class UserLoginForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('email', 'password')
        labels  = {
            'email': 'E-mail',
            'password': 'Password',
        }
