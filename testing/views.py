from django.shortcuts import render, redirect
from testing.models import Test, Question, Answer, Link, UserTest
from django.contrib.auth.models import User
from django.core.paginator import Paginator

"""
List of functions for testing
"""

"""
Function for rendering of tests list
"""
def SelectTest(request):
    tests = Test.objects.all()
    pages = Paginator(tests, 3)
    context = {'tests_on_page': pages.get_page((request.GET.get('page')))}
    return render(request, 'select_test.html', context)

"""
Function for rendering of information about test
"""
def TestInfo(request, test_id):
    questions = Link.objects.filter(test_id=test_id)
    test = Test.objects.get(id = test_id)
    user_test, ss = UserTest.objects.get_or_create(user_id = request.user, test_id = test)
    first_question = questions[0].question_id_id
    all_points = test.max_points()
    context = {'test': test, 'count': len(questions), 'first_question': first_question, 'user_test': user_test, 'max_points': all_points}
    return render(request, 'test_info.html', context)

"""
Function for passing of test
"""
def Testing(request, test_id, question_id):
    print("&&&&&&&&&&&8888888888&&&&&&&&&&&&&&&&")
    #print(str(request.META['HTTP_REFERER']))
    print(request.http.REFERER)
    # if str(request.META['HTTP_REFERER']) == 'http://' + str(request.META['HTTP_HOST']) + '/test/' + str(test_id) + '/':
    #     print('************')


    if request.method == 'POST':
        print(request.POST)
        if not 'answer' in request.POST:
            return redirect('/test/'+test_id+'/'+question_id+'/')
        test = Test.objects.get(id = test_id)
        user_test, ss = UserTest.objects.get_or_create(user_id = request.user, test_id = test)
        question = Question.objects.get(id = question_id)
        question_right_answers = question.count_right_answers()
        answers_in_request = request.POST.getlist('answer')
        answers = []
        count_right_answers = 0
        if (len(answers_in_request) != 1):
            for answer in answers_in_request:
                 answers.append(Answer.objects.get(id = answer))
        else:
            answer = Answer.objects.get(id = request.POST['answer'])

        miss_answer_points = 0

        if (len(answers_in_request) != 1):
            for ans in answers:
                if ans.correct == True:
                    user_test.point += ans.point
                    miss_answer_points += ans.point
                    count_right_answers += 1
                    user_test.save()

                else:
                    user_test.point -= miss_answer_points
                    user_test.save()


            if count_right_answers == question_right_answers:
                user_test.right_answer +=1
            else:
                user_test.wrong_answer +=1


            print('1POINTS',user_test.point, 'RIGHT', user_test.right_answer)
            user_test.save()
            return redirect('/test/'+test_id+'/'+str(int(question_id)+1)+'/')

        else:
            if answer.correct == True:
                if question_right_answers == 1:
                    user_test.right_answer +=1
                else:
                    user_test.wrong_answer +=1
                user_test.point += answer.point
            else:
                user_test.wrong_answer +=1
            user_test.save()
            print('2POINTS',user_test.point, 'RIGHT', user_test.right_answer)
            return redirect('/test/'+test_id+'/'+str(int(question_id)+1)+'/')

    elif int(question_id) > len(Link.objects.filter(test_id=test_id)):
        links = Link.objects.filter(test_id = test_id)
        test = Test.objects.get(id = test_id)
        user_test = UserTest.objects.get(user_id = request.user, test_id = test)
        all_points = test.max_points()

        if(0.5 <= user_test.point/all_points and user_test.point/all_points < 0.7):
            print('11')
            user_test.mark = 3
            user_test.save()
        elif(0.7 <= user_test.point/all_points and user_test.point/all_points < 0.9):
            print('12')
            user_test.mark = 4
            user_test.save()
        elif(user_test.point/all_points >= 0.9):
            print('13')
            user_test.mark = 5
            user_test.save()
        else:
            user_test.mark = 2
            user_test.save()
        user_test.count_attempts += 1
        user_test.save()
        print('ALL', all_points, user_test.point , user_test.point/all_points)
        return redirect('/test/'+test_id+'/result/')

    else:
        test = Test.objects.get(id = test_id)
        user_test, ss = UserTest.objects.get_or_create(user_id = request.user, test_id = test)
        # print(question_id)
        # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        if (question_id == '1'):
            user_test.right_answer = 0
            user_test.wrong_answer = 0
            user_test.point = 0
            user_test.mark = 2
            user_test.save()
        print('AAAAAAA',user_test.point)

        questions = Link.objects.filter(test_id = test_id)
        test = Test.objects.get(id = test_id)
        current_question = questions[int(question_id)-1].question_id_id
        name_question = Question.objects.get(id = current_question)
        answers = Answer.objects.filter(question_id = current_question)
        count_correct_answers = 0
        for answer in answers:
            if(answer.correct):
                print(answer.correct)
                count_correct_answers = count_correct_answers + 1
        print(count_correct_answers)
        context = {'test': test, 'question_id': int(question_id)+1,
        'question': current_question, 'answers': answers, 'name_question': name_question,
        'count':count_correct_answers}
        return render(request, 'testing.html', context)

"""
Function for rendering of result information after testing
"""
def TestResult(request, test_id):
    test = Test.objects.get(id = test_id)
    user_test = UserTest.objects.get(user_id = request.user, test_id = test)
    all = user_test.right_answer + user_test.wrong_answer
    context = {'all':all,'test_name': test, 'right_answer':user_test.right_answer, 'point':user_test.point, 'mark':user_test.mark}
    return render(request, 'test_result.html', context)
